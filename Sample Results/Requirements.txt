1. Result Sheet Roll no. Wise
2. Result Sheet Percentage Wise
3. Overall Result
4. Stream Wise Performance Summary
5. Aggregate Wise Topper List
6. Stream Wise Topper List
7. Passed Student List
8. Failed Student List
9. Compartment Student List
10. Absent Student List
11. Subject Wise Highest Scores, Minimum Scores, Average Score, Summary
12. Section Wise Highest Scores, Minimum Scores, Average Score, Summary
13. Section Wise Subject Wise Summary, Highest and Average Scores With Teacher Specification
