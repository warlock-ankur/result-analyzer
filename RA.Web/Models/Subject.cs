//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated from a template.
//
//     Manual changes to this file may cause unexpected behavior in your application.
//     Manual changes to this file will be overwritten if the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace RA.Web.Models
{
    using System;
    using System.Collections.Generic;
    
    public partial class Subject
    {
        public Subject()
        {
            this.Enrollments = new HashSet<Enrollment>();
        }
    
        public int Id { get; set; }
        public string Title { get; set; }
        public string Year { get; set; }
        public string SubjectCode { get; set; }
    
        public virtual ICollection<Enrollment> Enrollments { get; set; }
        public virtual School School { get; set; }
    }
}
