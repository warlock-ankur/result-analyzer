﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using RA.Web.ViewModel;
using RA.Web.Models;

namespace RA.Web.App_Code
{
    public class UpdateSectionDetails
    {
        private UpdateSectionDetailsVM Model;
        private String UserId;
        private int SchoolId;
        private String Year;
        public UpdateSectionDetails(UpdateSectionDetailsVM model, String userId, String year)
        {
            Model = model;
            this.UserId = userId;
            this.Year = year;
            using (RAModelContainer context = new RAModelContainer())
            {
                SchoolId = context.SchoolIdMappings.Where(sm => sm.UserId == UserId).Single().SchoolId;
            }
        }

        public void Update()
        {
            using (RAModelContainer context = new RAModelContainer())
            {
                if (Model.ARS != null && Model.ARE != null && Model.ARS.CompareTo(Model.ARE) < 0)
                {
                    var enrollments = context.Schools.Find(SchoolId).Students.Where(s => s.Year == Year)
                                    .Where(s => s.RollNumber.CompareTo(Model.ARS) >= 0 && s.RollNumber.CompareTo(Model.ARE) <= 0)
                                    .SelectMany(s => s.Enrollments);

                    foreach (var enrollment in enrollments)
                        enrollment.Section.SectionName = "Section A";
                    
                    context.SaveChanges();
                }

                if (Model.BRS != null && Model.BRE != null && Model.BRS.CompareTo(Model.BRE) < 0)
                {
                    var enrollments = context.Schools.Find(SchoolId).Students.Where(s => s.Year == Year)
                                    .Where(s => s.RollNumber.CompareTo(Model.BRS) >= 0 && s.RollNumber.CompareTo(Model.BRE) <= 0)
                                    .SelectMany(s => s.Enrollments);

                    foreach (var enrollment in enrollments)
                        enrollment.Section.SectionName = "Section B";
                    
                    context.SaveChanges();
                }

                if (Model.CRS != null && Model.CRE != null && Model.CRS.CompareTo(Model.CRE) < 0)
                {
                    var enrollments = context.Schools.Find(SchoolId).Students.Where(s => s.Year == Year)
                                    .Where(s => s.RollNumber.CompareTo(Model.CRS) >= 0 && s.RollNumber.CompareTo(Model.CRE) <= 0)
                                    .SelectMany(s => s.Enrollments);
                     
                    foreach (var enrollment in enrollments)
                            enrollment.Section.SectionName = "Section C";

                        context.SaveChanges();
                }

                if (Model.DRS != null && Model.DRE != null && Model.DRS.CompareTo(Model.DRE) < 0)
                {
                    var enrollments = context.Schools.Find(SchoolId).Students.Where(s => s.Year == Year)
                                    .Where(s => s.RollNumber.CompareTo(Model.DRS) >= 0 && s.RollNumber.CompareTo(Model.DRE) <= 0)
                                    .SelectMany(s => s.Enrollments);

                    foreach (var enrollment in enrollments)
                        enrollment.Section.SectionName = "Section D";
                            
                    context.SaveChanges();
                }

                if (Model.ERS != null && Model.ERE != null && Model.ERS.CompareTo(Model.ERE) < 0)
                {
                    var enrollments = context.Schools.Find(SchoolId).Students.Where(s => s.Year == Year)
                                    .Where(s => s.RollNumber.CompareTo(Model.ERS) >= 0 && s.RollNumber.CompareTo(Model.ERE) <= 0)
                                    .SelectMany(s => s.Enrollments);

                    foreach (var enrollment in enrollments)
                        enrollment.Section.SectionName = "Section E";

                    context.SaveChanges();
                }

                if (Model.FRS != null && Model.FRE != null && Model.FRS.CompareTo(Model.FRE) < 0)
                {
                    var enrollments = context.Schools.Find(SchoolId).Students.Where(s => s.Year == Year)
                                .Where(s => s.RollNumber.CompareTo(Model.FRS) >= 0 && s.RollNumber.CompareTo(Model.FRE) <= 0)
                                .SelectMany(s => s.Enrollments);

                    foreach (var enrollment in enrollments)
                        enrollment.Section.SectionName = "Section F";

                    context.SaveChanges();
                }

                if (Model.GRS != null && Model.GRE != null && Model.GRS.CompareTo(Model.GRE) < 0)
                {
                    var enrollments = context.Schools.Find(SchoolId).Students.Where(s => s.Year == Year)
                                   .Where(s => s.RollNumber.CompareTo(Model.GRS) >= 0 && s.RollNumber.CompareTo(Model.GRE) <= 0)
                                   .SelectMany(s => s.Enrollments);

                    foreach (var enrollment in enrollments)
                        enrollment.Section.SectionName = "Section G";

                    context.SaveChanges();
                }

                if (Model.HRS != null && Model.HRE != null && Model.HRS.CompareTo(Model.HRE) < 0)
                {
                    var enrollments = context.Schools.Find(SchoolId).Students.Where(s => s.Year == Year)
                                   .Where(s => s.RollNumber.CompareTo(Model.HRS) >= 0 && s.RollNumber.CompareTo(Model.HRE) <= 0)
                                   .SelectMany(s => s.Enrollments);

                    foreach (var enrollment in enrollments)
                        enrollment.Section.SectionName = "Section H";

                    context.SaveChanges();
                }

                if (Model.IRS != null && Model.IRE != null && Model.IRS.CompareTo(Model.IRE) < 0)
                {
                    var enrollments = context.Schools.Find(SchoolId).Students.Where(s => s.Year == Year)
                                   .Where(s => s.RollNumber.CompareTo(Model.IRS) >= 0 && s.RollNumber.CompareTo(Model.IRE) <= 0)
                                   .SelectMany(s => s.Enrollments);

                    foreach (var enrollment in enrollments)
                        enrollment.Section.SectionName = "Section I";

                    context.SaveChanges();
                }

                if (Model.JRS != null && Model.JRE != null && Model.JRS.CompareTo(Model.JRE) < 0)
                {
                    var enrollments = context.Schools.Find(SchoolId).Students.Where(s => s.Year == Year)
                                   .Where(s => s.RollNumber.CompareTo(Model.JRS) >= 0 && s.RollNumber.CompareTo(Model.JRE) <= 0)
                                   .SelectMany(s => s.Enrollments);

                    foreach (var enrollment in enrollments)
                        enrollment.Section.SectionName = "Section J";

                    context.SaveChanges();
                }

                if (Model.KRS != null && Model.KRE != null && Model.KRS.CompareTo(Model.KRE) < 0)
                {
                    var enrollments = context.Schools.Find(SchoolId).Students.Where(s => s.Year == Year)
                                   .Where(s => s.RollNumber.CompareTo(Model.KRS) >= 0 && s.RollNumber.CompareTo(Model.KRE) <= 0)
                                   .SelectMany(s => s.Enrollments);

                    foreach (var enrollment in enrollments)
                        enrollment.Section.SectionName = "Section K";

                    context.SaveChanges();
                }
            }
        }

    }
}