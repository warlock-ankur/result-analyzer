﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using RA.Web.ViewModel;
using RA.Web.Models;

namespace RA.Web.App_Code
{
    public class PrepareSchoolSummary
    {
        private String _UserId;
        private String year;
        
        public PrepareSchoolSummary(String UserId, String year)
        {
            this._UserId = UserId;
            this.year = year;
        }

        public  SchoolSummaryVM prepare()
        {
            SchoolSummaryVM model = new SchoolSummaryVM();
            using (RAModelContainer context = new RAModelContainer())
            {
                var schoolMapping = context.SchoolIdMappings.Where(s => s.UserId == _UserId).Single();
                var students = context.Students.Where(s => (s.School.Id == schoolMapping.SchoolId && s.Year == year));
                model.NumberOfStudentsAppeared = students.Count();
                model.NumberOfStudentsPassed = students.Where(s => s.Status == "PASS").Count();
                model.NumberOfStudentsFailed = students.Where(s => s.Status == "FAIL").Count();
                model.NumberOfStudentsAbsent = students.Where(s => s.Status == "ABST").Count();
                model.NumberOfStudentsComparted = students.Where(s => s.Status == "COMP").Count();
                model.PassPercent = (model.NumberOfStudentsPassed * 100) / model.NumberOfStudentsAppeared ;
                model.FailPercent = (model.NumberOfStudentsFailed * 100) / model.NumberOfStudentsAppeared;
                model.Year = year;
            }
            
            return model;
        }

    }
}