﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using RA.Web.ViewModel;
using RA.Web.Models;
using System.IO;

namespace RA.Web.App_Code
{
    public class PrepareSubjectSummary
    {
        private String UserId;
        private int SchoolId;
        private string year;
        public PrepareSubjectSummary(String UserId, string year)
        {
            this.UserId = UserId;
            this.year = year;
            using (RAModelContainer context = new RAModelContainer())
            {
                SchoolId = context.SchoolIdMappings.Where(sm => sm.UserId == UserId).Single().SchoolId;
            }
        }

        public IEnumerable<String> GetSubjectCodes()
        {
            using(RAModelContainer context = new RAModelContainer())
            {
                var subjectCodes = context.Schools.Find(SchoolId).Subjects.Where(s => s.Year == year)
                                    .Select(s => s.SubjectCode).Distinct().ToList();

                for (int i = 0; i < subjectCodes.Count(); i++)
                {
                    subjectCodes[i] = subjectCodes[i] + " " + GetSubjectTitle(subjectCodes[i]);
                }

                return subjectCodes;
            }
        }

        internal SubjectSummaryVM prepare(String SubjectCode)
        {
            SubjectSummaryVM model = new SubjectSummaryVM();

            using (RAModelContainer context = new RAModelContainer())
            {
                SchoolId = context.SchoolIdMappings.Where(sm => sm.UserId == UserId).Single().SchoolId;
                School school = context.Schools.Find(SchoolId);
                IEnumerable<Enrollment> enrollments = school.Enrollments.Where(e => e.Subject.SubjectCode == SubjectCode).Where(e => (e.Grade != "-" && e.Year == year));

                model.SubjectCode = SubjectCode;
                model.MaxScored = int.Parse(enrollments.Select(e => e.Score).Max(), System.Globalization.NumberStyles.Integer);
                model.MinScored = int.Parse(enrollments.Select(e => e.Score).Min(), System.Globalization.NumberStyles.Integer);
                model.NoOfStudentsAbove90 = enrollments.Where(e => e.Score.CompareTo("090") > 0).Count();
                model.NoOfStudentsBtw90And80 = enrollments.Where(e => (e.Score.CompareTo("090") <= 0 && e.Score.CompareTo("080") > 0)).Count();
                model.NoOfStudentsBtw80And70 = enrollments.Where(e => (e.Score.CompareTo("080") <= 0 && e.Score.CompareTo("070") > 0)).Count();
                model.NoOfStudentsBtw70And60 = enrollments.Where(e => (e.Score.CompareTo("070") <= 0 && e.Score.CompareTo("060") > 0)).Count();
                model.NoOfStudentsBtw60And50 = enrollments.Where(e => (e.Score.CompareTo("060") <= 0 && e.Score.CompareTo("050") > 0)).Count();
                model.NoOfStudentsBtw50And40 = enrollments.Where(e => (e.Score.CompareTo("050") <= 0 && e.Score.CompareTo("040") > 0)).Count();
                model.NoOfStudentsBelow40 = enrollments.Where(e => e.Score.CompareTo("040") <= 0).Count();
                model.SubjectTitle = GetSubjectTitle(SubjectCode);
                model.Year = year;
            }
            return model;
        }

        public String GetSubjectTitle(String SubjectCode)
        {
            var path = HttpRuntime.AppDomainAppPath + "/Resources/SubjectCodes.txt";
            String[] lines = File.ReadAllLines(path);
            foreach (var line in lines)
            {
                string[] tokens = line.Split(' ');
                if (tokens.Length > 1 && tokens[0].CompareTo(SubjectCode) == 0)
                {
                    return tokens[1];
                }
            }

            return "Subject Title Not Found";
        }
    }
}