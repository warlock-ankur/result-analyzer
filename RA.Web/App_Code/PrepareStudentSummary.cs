﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Web;
using System.Threading.Tasks;
using RA.Web.Models;
using RA.Web.ViewModel;

namespace RA.Web.App_Code
{
    public class PrepareStudentSummary
    {
        private int SchoolId;
        private String year;
        public PrepareStudentSummary(String UserId, String year)
        {
            using(RAModelContainer context = new RAModelContainer())
            {
                SchoolId = context.SchoolIdMappings.Where(sm => sm.UserId == UserId).Single().SchoolId;
            }
            this.year = year;
        }

        public List<StudentSummaryVM> prepare()
        {
            List<StudentSummaryVM> model = new List<StudentSummaryVM>();
            using(RAModelContainer context = new RAModelContainer())
            {
                foreach (var student in context.Students.Where(s => (s.School.Id == SchoolId && s.Year == year)).OrderByDescending(s => s.Percentage))
                {
                    StudentSummaryVM ss = new StudentSummaryVM();
                    ss.SubjectCodes = new List<string>();
                    ss.Marks = new List<string>();
                    ss.Grades = new List<string>();

                    ss.Result = student.Status;
                    ss.INT1 = student.INT1;
                    ss.INT2 = student.INT2;
                    ss.INT3 = student.INT3;
                    ss.Name = student.Name;
                    ss.RollNumber = student.RollNumber;
                    ss.Year = student.Year;
                    ss.Percentage = student.Percentage;

                    var enrollments = student.Enrollments.OrderBy(e => e.Subject.SubjectCode);
                    foreach (var enrollment in enrollments)
                    {
                        ss.SubjectCodes.Add(enrollment.Subject.SubjectCode);
                        ss.Marks.Add(enrollment.Score);
                        ss.Grades.Add(enrollment.Grade);
                    }

                    model.Add(ss);
                }
            }
            return model;
        }
    }
}