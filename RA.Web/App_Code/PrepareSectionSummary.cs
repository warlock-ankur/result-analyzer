﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using RA.Web.Models;
using RA.Web.ViewModel;
using RA.Web;
using System.IO;

namespace RA.Web.App_Code
{
    public class PrepareSectionSummary
    {
        private int SchoolId;
        private String Year;
        public PrepareSectionSummary(String UserId, String Year)
        {
            using(RAModelContainer context = new RAModelContainer())
            {
                SchoolId = context.SchoolIdMappings.Where(sm => sm.UserId == UserId).Single().SchoolId;
            }
            this.Year = Year;
        }

        public List<String> getSections()
        {
            List<String> sections;
            using(RAModelContainer context = new RAModelContainer())
            {
                var sectionNames = context.Schools.Find(SchoolId).Enrollments.Where(e => e.Year == Year)
                                    .Select(s => s.Section.SectionName).Distinct();

                sections = sectionNames.ToList();
            }

            return sections;
        }
        public SectionSummaryVM prepare(String sectionName)
        {
            SectionSummaryVM model = new SectionSummaryVM();
            using (RAModelContainer context = new RAModelContainer())
            {
                var school = context.Schools.Find(SchoolId);
                var students = school.Students.Where(s => s.Year == Year && s.Section.SectionName == sectionName);

                model.SectionName = sectionName;
                model.NoOfStudentsEnrolled = students.Count();
                model.NoOfStudentsPassed = students.Where(s => s.Status == "PASS").Count();
                model.NoOfStudentsFailed = students.Where(s => s.Status == "FAIL").Count();
                model.NoOfStudentsAbsent = students.Where(s => s.Status == "ABST").Count();
                model.NoOfStudentscomparted = students.Where(s => s.Status == "COMP").Count();
                model.Year = Year;
                model.HightestScoreDetails = new List<HighestScoreDetailVM>();

                var enrollments = students.SelectMany(s => s.Enrollments);
                var subjectCodes = enrollments.Select(e => e.Subject.SubjectCode).Distinct();
                foreach (var subjectCode in subjectCodes)
                {
                    var score = enrollments.Where(e => e.Subject.SubjectCode == subjectCode).Select(e => e.Score).Max();
                    var HighestScorestudents = enrollments.Where(e => e.Score == score).Select(e => e.Student);
                    HighestScoreDetailVM hsd = new HighestScoreDetailVM();
                    foreach (var student in HighestScorestudents)
                    {
                        hsd.StudentName = student.Name;
                        hsd.StudentRollNumber = student.RollNumber;
                        hsd.SubjectCode = subjectCode;
                        hsd.SubjectTitle = GetSubjectTitle(subjectCode);
                        hsd.Score = score;
                    }
                    model.HightestScoreDetails.Add(hsd);
                }
            }

            return model;
        }

        public String GetSubjectTitle(String SubjectCode)
        {
            var path = HttpRuntime.AppDomainAppPath + "/Resources/SubjectCodes.txt";
            String[] lines = File.ReadAllLines(path);
            foreach (var line in lines)
            {
                string[] tokens = line.Split(' ');
                if(tokens.Length > 1 && tokens[0].CompareTo(SubjectCode) == 0)
                {
                    return tokens[1];
                }
            }

            return "Subject Title Not Found";
        }
    }
}