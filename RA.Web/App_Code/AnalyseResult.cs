﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Web;
using System.Threading.Tasks;
using RA.Web.Models;

namespace RA.Web.App_Code
{
    public class AnalyseResult
    {
        private String UserId;
        private int schoolId;
        private School school;
        private String year;

        public AnalyseResult(String userId, String year)
        {
            this.UserId = userId;
            using (RAModelContainer context = new RAModelContainer())
            {
                schoolId = context.SchoolIdMappings.Where(sm => sm.UserId == this.UserId).Single().SchoolId;
                school = context.Schools.Find(schoolId);
            }
            this.year = year;
        }

        public void BeginAnalysing(string path)
        {
            string[] lines = File.ReadAllLines(path);
            foreach (string line in lines)
            {
                if (line.Length > 0)
                {
                    if (char.IsDigit(line[0]))
                    {
                        ProcessLine(line);
                    }
                }
            }
             
            File.Delete(path);
        }

        private void ProcessLine(string line)
        {
            Student student;
            Subject subject;
            Enrollment enrollment;
            Section section;

            String[] delimeter = { " " };
            string[] tokens = line.Split(delimeter, StringSplitOptions.RemoveEmptyEntries);

            if (tokens[tokens.Length - 1] == "ABST") 
            {
                StudentAbsent(line);
                return;
            }

            using (RAModelContainer context = new RAModelContainer())
            {
                int i = 2;
                String studentName = tokens[1];
                while (char.IsLetter(tokens[i][0]))
                {
                    studentName += " " + tokens[i];
                    i++;
                }

                school = context.Schools.Find(schoolId);
                section = context.Sections.Add(new Section() 
                {
                    SectionName = "Not Defined",
                    School = school,
                });

                student = context.Students.Add(new Student()
                {
                    Name = studentName,
                    RollNumber = tokens[0],
                    School = school,
                    Year = this.year,
                    Status = tokens[tokens.Length - 1],
                    INT3 = tokens[tokens.Length - 2],
                    INT2 = tokens[tokens.Length - 3],
                    INT1 = tokens[tokens.Length - 4],
                    Section = section,
                    Percentage = "0"
                });

                double per = 0.0;
                int c = 0;
                while (i < tokens.Length - 4)
                {
                    subject = context.Subjects.Add(new Subject()
                    {
                        SubjectCode = tokens[i],
                        Title = "Not Implemented",
                        Year = this.year,
                        School = school,
                    });

                    var grade = tokens[i + 2];
                    string score = null;
                    if (grade == "E")
                        score = tokens[i + 1][0].ToString() + tokens[i + 1][1].ToString() + tokens[i + 1][2];
                    else
                        score = tokens[i + 1];

                    enrollment = context.Enrollments.Add(new Enrollment()
                    {
                        Grade = grade,
                        Score = score,
                        Year = year,
                        School = school,
                        Student = student,
                        Subject = subject,
                        Section = section
                    });

                    per += double.Parse(score);
                    c++;

                    student.Enrollments.Add(enrollment);
                    subject.Enrollments.Add(enrollment);
                    section.Enrollments.Add(enrollment);
                    section.Students.Add(student);
                    context.Schools.Find(schoolId).Enrollments.Add(enrollment);
                    context.Schools.Find(schoolId).Students.Add(student);
                    context.Schools.Find(schoolId).Subjects.Add(subject);
                    
                    context.SaveChanges();
                    
                    i += 3;    
                }
                if (c == 5)
                    per = per / 5;
                else
                    per = per / 6;

                student.Percentage = Math.Round(per, 2).ToString();
                context.SaveChanges();
            }
        }

        private void StudentAbsent(string line)
        {
            Student student;
            Subject subject;
            Enrollment enrollment;
            Section section;

            String[] delimeter = { " " };
            string[] tokens = line.Split(delimeter, StringSplitOptions.RemoveEmptyEntries);

            using (RAModelContainer context = new RAModelContainer())
            {
                String studentName = tokens[1];
                int i = 2;
                while (char.IsLetter(tokens[i][0]))
                {
                    studentName += " " + tokens[i];
                    i++;
                }

                school = context.Schools.Find(schoolId);
                section = context.Sections.Add(new Section() 
                {
                    SectionName = "Not Defined",
                    School = school
                });

                student = context.Students.Add(new Student()
                {
                    Name = studentName,
                    RollNumber = tokens[0],
                    Status = "ABST",
                    Year = year,
                    INT1 = "-",
                    INT2 = "-",
                    INT3 = "-",
                    School = school,
                    Section = section,
                    Percentage = "-"
                });

                while(i < tokens.Length - 1)
                {
                    subject = context.Subjects.Add(new Subject()
                    {
                        School = school,
                        SubjectCode = tokens[i],
                        Title = "NOT Implemented Absent",
                        Year = year,
                    });

                    enrollment = context.Enrollments.Add(new Enrollment()
                    {
                        Grade = "-",
                        Score = "-",
                        Student = student,
                        Subject = subject,
                        Year = year,
                        School = school,
                        Section = section
                    });

                    student.Enrollments.Add(enrollment);
                    subject.Enrollments.Add(enrollment);
                    section.Enrollments.Add(enrollment);
                    section.Students.Add(student);
                    context.Schools.Find(schoolId).Enrollments.Add(enrollment);
                    context.Schools.Find(schoolId).Subjects.Add(subject);

                    context.SaveChanges();

                    i++;
                }
            }   
        }
    }
}