﻿using Microsoft.Owin;
using Owin;

[assembly: OwinStartupAttribute(typeof(RA.Web.Startup))]
namespace RA.Web
{
    public partial class Startup
    {
        public void Configuration(IAppBuilder app)
        {
            ConfigureAuth(app);
        }
    }
}
