﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace RA.Web.ViewModel
{
    public class SchoolSummaryVM
    {
        public String Year { get; set; }
        public int NumberOfStudentsAppeared { get; set; }

        public int NumberOfStudentsPassed { get; set; }

        public int NumberOfStudentsFailed { get; set; }

        public int NumberOfStudentsAbsent { get; set; }

        public int NumberOfStudentsComparted { get; set; }
        
        public double PassPercent { get; set; }
    
        public double FailPercent { get; set; }
    }
}