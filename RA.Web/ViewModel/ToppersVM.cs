﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using RA.Web.Models;

namespace RA.Web.ViewModel
{
    public class ToppersVM
    {
        public List<Student> SchoolTopper { get; set; }

        public List<KeyValuePair<String, List<TopperStudent>>> SubjectToppers { get; set; }
    }


}