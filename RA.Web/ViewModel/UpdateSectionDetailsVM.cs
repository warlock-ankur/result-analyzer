﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace RA.Web.ViewModel
{
    public class UpdateSectionDetailsVM
    {
        public String ARS { get; set; }

        public String ARE { get; set; }

        public String BRS { get; set; }

        public String BRE { get; set; }

        public String CRS { get; set; }

        public String CRE { get; set; }

        public String DRS { get; set; }

        public String DRE { get; set; }

        public String ERS { get; set; }

        public String ERE { get; set; }

        public String FRS { get; set; }

        public String FRE { get; set; }

        public String GRS { get; set; }

        public String GRE { get; set; }

        public String HRS { get; set; }

        public String HRE { get; set; }

        public String IRS { get; set; }

        public String IRE { get; set; }

        public String JRS { get; set; }

        public String JRE { get; set; }

        public String KRS { get; set; }

        public String KRE { get; set; }
    }
}