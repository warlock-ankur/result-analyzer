﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace RA.Web.ViewModel
{
    public class SubjectSummaryVM
    {
        public String SubjectTitle { get; set; }

        public String SubjectCode { get; set; }

        public int MaxScored { get; set; }

        public int MinScored { get; set; }

        public int NoOfStudentsAbove90 { get; set; }

        public int NoOfStudentsBtw90And80 { get; set; }

        public int NoOfStudentsBtw80And70 { get; set; }

        public int NoOfStudentsBtw70And60 { get; set; }

        public int NoOfStudentsBtw60And50 { get; set; }

        public int NoOfStudentsBtw50And40 { get; set; }

        public int NoOfStudentsBelow40 { get; set; }

        public int NoOfStudentsWithDistinction { get; set; }

        public String Year { get; set; }
    }
}