﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace RA.Web.ViewModel
{
    public class TopperStudent
    {
        public String RollNumber { get; set; }

        public String Name { get; set; }

        public String Score { get; set; }

        public String OverallPercentage { get; set; }

        public String Result { get; set; }
    }
}
