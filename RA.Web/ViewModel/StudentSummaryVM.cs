﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace RA.Web.ViewModel
{
    public class StudentSummaryVM
    {
        public String RollNumber { get; set; }

        public String Name { get; set; }

        public List<String> SubjectCodes { get; set; }

        public List<String> Marks { get; set; }

        public List<String> Grades { get; set; }

        public String INT1 { get; set; }

        public String INT2 { get; set; }

        public String INT3 { get; set; }

        public String Percentage { get; set; }

        public String Result { get; set; }

        public String Year { get; set; }
    }
}