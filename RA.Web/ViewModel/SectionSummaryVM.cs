﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace RA.Web.ViewModel
{
    public class SectionSummaryVM
    {
        public String SectionName { get; set; }

        public int NoOfStudentsEnrolled { get; set; }

        public int NoOfStudentsPassed { get; set; }

        public int NoOfStudentsFailed { get; set; }

        public int NoOfStudentsAbsent { get; set; }

        public int NoOfStudentscomparted { get; set; }

        public String Year { get; set; }

        public List<HighestScoreDetailVM> HightestScoreDetails { get; set; }
    }

}