﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace RA.Web.ViewModel
{
    public class HighestScoreDetailVM
    {
        public String SubjectCode { get; set; }

        public String SubjectTitle { get; set; }

        public String Score { get; set; }

        public String StudentName { get; set; }

        public String StudentRollNumber { get; set; }
    }
}