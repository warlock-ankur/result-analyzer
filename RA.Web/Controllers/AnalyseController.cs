﻿using Microsoft.AspNet.Identity;
using RA.Web.App_Code;
using RA.Web.Models;
using RA.Web.ViewModel;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Threading.Tasks;
using System.Web;
using System.Web.Mvc;

namespace RA.Web.Controllers
{
    [Authorize]
    public class AnalyseController : Controller
    {
        private String getYear()
        {
            if (Session["year"] == null)
            {
                if (Response.Cookies.Get("year").Value == null)
                {
                    Response.Cookies.Add(new HttpCookie("year", DateTimeOffset.Now.Year.ToString()));
                    Session["year"] = DateTime.Now.Year;
                }
                else
                    Session["year"] = Response.Cookies.Get("year").Value;
            }

            return Session["year"].ToString();
        }

        //
        // GET: /Analyse/
        public ActionResult Index()
        {
            return View();
        }

        [HttpPost]
        public ActionResult Index(String year)
        {
            Session["year"] = year;
            Response.Cookies.Add(new HttpCookie("year", year));
            return View();
        }

        //
        // GET: /Analyse/Upload
        public ActionResult Upload()
        {
            ViewBag.Message = "Welcome to result Uploader";
            return View();
        }


        //
        // POST: /Analyse/Upload
        [HttpPost]
        public ActionResult Upload(HttpPostedFileBase file, String year)
        {
            if (ModelState.IsValid)
            {
                try
                {
                    var intyear = int.Parse(year);
                } catch(Exception ex)
                {
                    year = System.DateTime.Now.Year.ToString();
                }

                if (file != null)
                {
                    var fileName = User.Identity.Name + Path.GetFileName(file.FileName);
                    var path = Path.Combine(Server.MapPath("~/App_Data/uploads"), fileName);
                    file.SaveAs(path);
                    AnalyseResult ar = new AnalyseResult(User.Identity.GetUserId(), year);
                    ar.BeginAnalysing(path);
                    ViewBag.Message = "Analysis Successful";
                    return View();
                }
                else
                {
                    ViewBag.Message = "Please Choose a File";
                    return View();
                }
            }

            // If We reached here Something Went wrong
            ViewBag.Message = "Something Went Wrong! Please Try Again";
            return View();
        }


        //
        // GET: School Summary
        public ActionResult SchoolSummary()
        {

            PrepareSchoolSummary pss = new PrepareSchoolSummary(User.Identity.GetUserId(), getYear());
            SchoolSummaryVM model = pss.prepare();
            return View(model);
        }

        //
        // GET: /Analyse/SubjectSumamry
        public ActionResult SubjectSummary()
        {
            PrepareSubjectSummary pss = new PrepareSubjectSummary(User.Identity.GetUserId(), getYear());
            IEnumerable<String> subjectCodes = pss.GetSubjectCodes();
            ViewBag.subjectCodes = subjectCodes;
            return View();
        }

        //
        // POST: /Analyse/SubjectSummary
        [HttpPost]
        public ActionResult SubjectSummary(String SubjectCode)
        {
            PrepareSubjectSummary pss = new PrepareSubjectSummary(User.Identity.GetUserId(), getYear());
            var tokens = SubjectCode.Split(' ');
            return View(pss.prepare(tokens[0]));
        }

        public ActionResult StudentSummary()
        {
            PrepareStudentSummary pss = new PrepareStudentSummary(User.Identity.GetUserId(), getYear());
            return View(pss.prepare());
        }

        public ActionResult SectionSummary()
        {
            PrepareSectionSummary pss = new PrepareSectionSummary(User.Identity.GetUserId(), getYear());
            List<String> sections = pss.getSections();
            ViewBag.sectionnames = sections;
            return View();
        }

        [HttpPost]
        public ActionResult SectionSummary(String Section)
        {
            PrepareSectionSummary pss = new PrepareSectionSummary(User.Identity.GetUserId(), getYear());
            return View(pss.prepare(Section));
        }

        public ActionResult UpdateSectionDetails()
        {
            return View();
        }

        [HttpPost]
        public ActionResult UpdateSectionDetails(UpdateSectionDetailsVM model)
        {
            UpdateSectionDetails usd = new UpdateSectionDetails(model, User.Identity.GetUserId(),getYear());
            usd.Update();

            return RedirectToAction("Index");
        }

        public ActionResult Toppers()
        {
            PrepareToppers pt = new PrepareToppers(User.Identity.GetUserId(), getYear());
            return View(pt.prepare());
        }
    }
}